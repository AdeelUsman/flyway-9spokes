CREATE TABLE `RPT_9spokes` (
  `CompanyId` char(36) DEFAULT NULL,
  `CompanyName` varchar(1024) DEFAULT NULL,
  `CompanyDesc` varchar(1024) DEFAULT NULL,
  `IndustryName` varchar(45) DEFAULT NULL,
  `RegistrationDate` datetime DEFAULT NULL,
  `Channel` varchar(20) DEFAULT NULL,
  `Isactive` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `RPT_Barclays` (
  `CompanyId` char(36) DEFAULT NULL,
  `CompanyName` varchar(1024) DEFAULT NULL,
  `CompanyDesc` varchar(1024) DEFAULT NULL,
  `IndustryName` varchar(45) DEFAULT NULL,
  `RegistrationDate` datetime DEFAULT NULL,
  `Channel` varchar(20) DEFAULT NULL,
  `Isactive` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `RPT_Deloitteuk` (
  `CompanyId` char(36) DEFAULT NULL,
  `CompanyName` varchar(1024) DEFAULT NULL,
  `CompanyDesc` varchar(1024) DEFAULT NULL,
  `IndustryName` varchar(45) DEFAULT NULL,
  `RegistrationDate` datetime DEFAULT NULL,
  `Channel` varchar(20) DEFAULT NULL,
  `Isactive` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `RPT_User_9spokes` (
  `UserId` varchar(36) DEFAULT NULL,
  `CompanyId` varchar(36) DEFAULT NULL,
  `UserResgistrationDate` datetime DEFAULT NULL,
  `LastLoginDate` datetime DEFAULT NULL,
  `displayname` varchar(45) DEFAULT NULL,
  `Channel` varchar(7) DEFAULT NULL,
  `Role` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

